using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using Core.Assets;
using Core.Assets.Models;
using Core.Database;
using Core.Database.Payloads;
using Core.Database.QueryLanguages;
using Core.Database.Tables;
using Core.Relationships;
using Core.Users.Implementation.QueryLanguages;
using GraphQL;
using GraphQL.EntityFramework;
using GraphQL.Server;
using GraphQL.Types;
using GraphQL.Utilities;
using MediatR;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Server.Kestrel.Core;
using Microsoft.AspNetCore.SpaServices.ReactDevelopmentServer;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace Web
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            Core.Database.BeawreContext.ConnectionString = Configuration.GetConnectionString("DefaultConnection");

            services.AddMediatR();

            services.AddScoped<IBeawreContext, BeawreContext>();
            services.AddDbContext<BeawreContext>();

            #region GraphQl
            GraphTypeTypeRegistry.Register<User, UserGraphQl>();
            GraphTypeTypeRegistry.Register<Asset, AssetGraphQl>();
            GraphTypeTypeRegistry.Register<AssetModel, AssetGraphQl>();
            GraphTypeTypeRegistry.Register<Vulnerability, VulnerabilityGraphQl>();
            GraphTypeTypeRegistry.Register<Risk, RisksGraphQl>();
            GraphTypeTypeRegistry.Register<Treatment, TreatmentsGraphQl>();
            GraphTypeTypeRegistry.Register<Relationship, RelationshipGraphQl>();
            GraphTypeTypeRegistry.Register<Evidence, EvidenceGraphQl>();
            GraphTypeTypeRegistry.Register<RiskPayload, RiskPayloadGraphQl>();
            GraphTypeTypeRegistry.Register<RiskPayloadModel, RiskPayloadModelGraphQl>();
            GraphTypeTypeRegistry.Register<TreatmentPayloadModel, TreatmentPayloadModel.TreatmentkPayloadGraphQl>();
            GraphTypeTypeRegistry.Register<OwaspDictionary, OwaspDictionaryGraphType>();
            GraphTypeTypeRegistry.Register<List<OwaspDictionary>, ListGraphType<OwaspDictionaryGraphType>>();

            EfGraphQLConventions.RegisterInContainer(services, new BeawreContext(), userContext => (BeawreContext)userContext);

            services.AddSingleton<UserGraphQl>();
            services.AddSingleton<AssetGraphQl>();
            services.AddSingleton<VulnerabilityGraphQl>();
            services.AddSingleton<RisksGraphQl>();
            services.AddSingleton<TreatmentsGraphQl>();
            services.AddSingleton<RelationshipGraphQl>();
            services.AddSingleton<EvidenceGraphQl>();
            services.AddSingleton<RiskPayloadGraphQl>();
            services.AddSingleton<RiskPayloadModelGraphQl>();
            services.AddSingleton<TreatmentPayloadModel.TreatmentkPayloadGraphQl>();
            services.AddSingleton<OwaspDictionaryGraphType>();

            foreach (var type in GetGraphQlTypes())
                services.AddSingleton(type);

            services.AddGraphQL(options => options.ExposeExceptions = true);

            services.AddSingleton<IDocumentExecuter, EfDocumentExecuter>();
            services.AddSingleton<IDependencyResolver>(
                provider => new FuncDependencyResolver(provider.GetRequiredService));
            services.AddSingleton<ISchema, Controllers.Schema>();
            #endregion

            Core.Users.Config.InitializeServices(ref services);
            Core.Assets.Config.InitializeServices(ref services);
            Core.Relationships.Config.InitializeServices(ref services);
            Core.AuditTrail.Config.InitializeServices(ref services);

            services.Configure<CookiePolicyOptions>(options =>
            {
                options.CheckConsentNeeded = context => true;
                options.MinimumSameSitePolicy = SameSiteMode.None;
            });

            services.AddAutoMapper(opt =>
            {
                opt.AddProfile(new Core.Users.UsersProfile());
                opt.AddProfile(new AssetsProfile());
                opt.AddProfile(new RelationshipProfile());
                opt.AddProfile(new Core.AuditTrail.CustomProfile());
            });

            services.AddCors(opt =>
            {
                opt.AddPolicy("CorsRules", pol => pol.WithOrigins("http://localhost:5410").AllowAnyMethod().AllowAnyHeader().AllowCredentials());
            });

            services.Configure<IISServerOptions>(opt =>
            {
                opt.AllowSynchronousIO = true;
            });

            services.Configure<KestrelServerOptions>(opt =>
            {
                opt.AllowSynchronousIO = true;
            });

            services.AddMvc()
                .AddNewtonsoftJson();

            services.AddSpaStaticFiles(configuration =>
            {
                configuration.RootPath = "ClientApp/build";
            });
        }



        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Error");
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseStaticFiles();
            app.UseSpaStaticFiles();

            app.UseAuthentication();

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller}/{action=Index}/{id?}");
            });

            app.UseSpa(spa =>
            {
                spa.Options.SourcePath = "ClientApp";
            });

            BeawreContext.Initalize();
        }

        static IEnumerable<Type> GetGraphQlTypes()
        {
            return typeof(Startup).Assembly
                .GetTypes()
                .Where(x => !x.IsAbstract &&
                            (typeof(IObjectGraphType).IsAssignableFrom(x) ||
                             typeof(IInputObjectGraphType).IsAssignableFrom(x)));
        }

        //private static IEdmModel GetEdmModel()
        //{
        //    ODataConventionModelBuilder builder = new ODataConventionModelBuilder();
        //    builder.EntitySet<Asset>("Assets");
        //    builder.EntitySet<User>("Users");
        //    return builder.GetEdmModel();
        //}
    }
}
