import React from "react";
import { Menu, Icon } from "antd";
import { withTranslation } from "react-i18next";
import { Link } from "react-router-dom";

class MainMenu extends React.Component {
  render() {
    const appConfig = JSON.parse(localStorage.getItem("appConfig"));
    return (
      <Menu theme="light" mode="inline" style={{ borderRightColor: "transparent", paddingTop: 8 }}>
        {appConfig.router.menus.map(e => (
          <Menu.Item key={e.key}>
            <Link to={e.to}>
              <Icon type={e.icon} />
              <span>{this.props.t(e.title)}</span>
            </Link>
          </Menu.Item>
        ))}
      </Menu>
    );
  }
}

export default withTranslation()(MainMenu);
